pragma solidity >=0.4.21 <0.6.0;

import "./IBankWallet.sol";
import "./SafeMath.sol";
import "./Ownable.sol";

contract BankWallet is IBankWallet, Ownable {

     using SafeMath for uint256;
     
     uint256 totalAmount;

     //event SetGasPrice(uint);
     event DepositEvent(address indexed fromAccount, uint256 amount);

     //uint256 currenttime = now;
     
     constructor() public {
         owner = msg.sender; // Owner of the contract
         // msg.value
         totalAmount = 0;
     }

    // Fallback function for direct transfer
    function() payable external {
        //revert();
        Deposit(msg.value);
        
    }

     // Function to deposit the Amount
     function Deposit(uint256 _amount) public {
         require(_amount >= 0);
         //balances[msg.sender] = balances[msg.sender] + _amount;
         balances[msg.sender] = balances[msg.sender].add(_amount);
         totalAmount = totalAmount.add(_amount);

        emit DepositEvent(msg.sender, _amount);

     }
     
     // Function to Withdarw the Amount
     function Withdraw(uint256 _amount) public returns (uint){
         
         require(balances[msg.sender] >= _amount);
         
         //if(balances[msg.sender] >= _amount){
         //   balances[msg.sender] = balances[msg.sender] - _amount;
         //}
         
         //balances[msg.sender] = balances[msg.sender] - _amount;
         balances[msg.sender] = balances[msg.sender].sub(_amount);
         totalAmount = totalAmount.sub(_amount);
         
         //emit SetGasPrice(tx.gasprice);

         return balances[msg.sender];
         
     }
    
    // Function to return the Balance of the Caller
    function GetBalance() public view returns (uint256) {
        return balances[msg.sender];
    }
    
    // Should not be executed by everyone - can be executed by only owner
    function GetBankBalance() public view onlyOwner returns (uint256){
        return totalAmount;
    }
    
    function Transfer(address _to, uint256 _amount) public returns (uint256) {
    
        require(_amount > 0);
        require(balances[msg.sender] >= _amount);
    
        balances[msg.sender] = balances[msg.sender].sub(_amount);
        balances[_to] = balances[_to].add(_amount);
        
        return balances[msg.sender];
    }
    
}
