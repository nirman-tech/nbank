import Web3 from "web3";
import BankWallet from "./contracts/BankWallet.json";

const options = {
  web3: {
    block: false,
    customProvider: new Web3("ws://localhost:8545"),
  },
  contracts: [BankWallet],
  events: {
    SimpleStorage: ["DepositEvent"],
  },
  polls: {
    accounts: 1500,
  },
};

export default options;
