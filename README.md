# nbank

A simplified banking services on ethereum to explore smart contract features.

**Local Setup:**

**Step-1: Install truffle using the command:** 

npm install -g truffle 

Pre-Requisits to Install Truffle 
* NodeJS v8.9.4 or Above 
* For windows machines there might be naming conflicts. Use Powershell for exploring truffle
 

Ref: [Trruffle Getting Started](https://www.trufflesuite.com/docs/truffle/getting-started/installation) 


**Step-2: Install Local Blockchain Emulator (TestRPC)**

npm install -g ganache-cli 

Ref: [Ganache CLI](https://docs.nethereum.com/en/latest/ethereum-and-clients/ganache-cli/) 

**Step-2A: Create Truffle Project**

Following script will assist in create a truffle Project 

mkdir EthereumSample 
cd  EthereumSample 

truffle init 

**Step-3: Install & Create a Drizzle (Optional)**

Drizzle is based on reactjs, if npm v5.2 and above is already installed then you don’t need to do anything, otherwise need to install reactjs globally with the following command: 

npm install -g create-react-app 


**Step-3A: Creating a React Project**

npx create-react-app client 

**Step-3B: Installing Drizzle** 

npm install drizzle 
 
Ref: [Getting Started With Drizzle](https://www.trufflesuite.com/tutorials/getting-started-with-drizzle-and-react) 

Ref: [Create React App](https://github.com/facebook/create-react-app/ )