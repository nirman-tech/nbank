"use strict";
var BankWallet = artifacts.require("./BankWallet.sol");

contract('BankWallet', function(accounts) {

    var bankWallet;
    
    before(async () => 
        {
            bankWallet      = await BankWallet.deployed();
        });

    async function testErrorRevert(prom)
    {
        let rezE = -1
        try { await prom }
        catch(e) {
            rezE = e.message.indexOf('revert') 
    //console.log("Error " + e)        
        }
        assert(rezE >= 0, "Must generate error and error message must contain revert");
    }


    it ("Test Case -1 : Deposit & Withdraw Tests", async function()
        { 

            let depositAmount = 1000

            await bankWallet.Deposit(depositAmount, {from:accounts[0]});
            let balance = await bankWallet.GetBalance({from:accounts[0]});
            assert.equal(balance, depositAmount)

            await bankWallet.Withdraw(depositAmount - 100, {from:accounts[0]});

            let balance2 = await bankWallet.GetBalance({from:accounts[0]});
            assert.equal(balance2, 100)

        });

    it ("Test Case -2 : Get Bank Balance by Non Owner", async function()
    { 

        //let banlBal = await bankWallet.GetBankBalance({from:accounts[1]});

        testErrorRevert(bankWallet.GetBankBalance({from:accounts[1]}));

    });

    // Write Test Case to do a transfer to another account

});